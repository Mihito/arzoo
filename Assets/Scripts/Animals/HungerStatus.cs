﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HungerStatus : MonoBehaviour
{
    public List<MonoBehaviour> ObjectsToDisable;

    private Coroutine hungerCoroutine;

    public void OnFeed()
    {
        Hide();
        StartNextHungerStatusCountdown();
    }

    public void StartNextHungerStatusCountdown()
    {
        if (hungerCoroutine != null)
        {
            StopCoroutine(hungerCoroutine);
        }

        float timeLeftToHungerNotification = Random.Range(PlaceForAnAnimal.MinHungerNotifictionDelay, PlaceForAnAnimal.MaxHungerNotifictionDelay);
        hungerCoroutine = StartCoroutine(HungerTimer(timeLeftToHungerNotification));
    }

    public void EnableHungerStatus()
    {
        Show();
    }

    public void Show()
    {
        foreach (var obj in ObjectsToDisable)
        {
            obj.enabled = true;
        }
    }

    public void Hide()
    {
        foreach (var obj in ObjectsToDisable)
        {
            obj.enabled = false;
        }
    }

    private IEnumerator HungerTimer(float timeLeftToHungerNotification)
    {
        yield return new WaitForSeconds(timeLeftToHungerNotification);
        EnableHungerStatus();
    }
}
