﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceForAnAnimal : MonoBehaviour
{
    public const float MinHungerNotifictionDelay = 4;
    public const float MaxHungerNotifictionDelay = 15;

    private Transform animalHere = null;
    private HungerStatus hungerStatus;

    public Transform GetAnimal()
    {
        return animalHere;
    }

    public void PutAnAnimal(Transform animalTransform)
    {
        animalHere = animalTransform;

        if (hungerStatus == null)
        {
            Vector3 positionToScreen = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 targetPosition = new Vector3(positionToScreen.x, positionToScreen.y + 50, positionToScreen.z);

            hungerStatus = Instantiate(AnimalsManager.Instance.HungerStatusPrefab, targetPosition, Quaternion.identity,
                AnimalsManager.Instance.HungerStatusesKeeper);
        }

        hungerStatus.Hide();
        hungerStatus.StartNextHungerStatusCountdown();
    }
}
