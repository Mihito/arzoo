﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{
    public void LoadColoringScene()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadZooScene()
    {
        SceneManager.LoadScene(1);
    }
}
