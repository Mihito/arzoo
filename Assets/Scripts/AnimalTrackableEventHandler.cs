﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalTrackableEventHandler : DefaultTrackableEventHandler
{
    [SerializeField] private AnimalsManager.Animal animalType;
    [SerializeField] private Transform model;
    [SerializeField] private GameObject renderCamera;

    protected override void OnTrackingFound()
    {
        var animalController = FindObjectOfType<AnimalController>();
        animalController.CurrentAnimalType = animalType;
        animalController.CurrentAnimalTransform = model;
        animalController.renderCamera = renderCamera;

        base.OnTrackingFound();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
    }
}