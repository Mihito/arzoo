﻿using System.IO;
using UnityEngine;

public class AnimalController : MonoBehaviour
{
    public Transform CurrentAnimalTransform;
    public AnimalsManager.Animal CurrentAnimalType;

    public GameObject renderCamera;

    private Renderer meshRenderer;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveCurrentTexture();
        }
    }

    public void SaveCurrentTexture()
    {
        print("Saving texture to: " + Application.persistentDataPath + AnimalsManager.FolderWithTextures);

        renderCamera.SetActive(false);

        meshRenderer = CurrentAnimalTransform.GetComponent<Renderer>();

        if (meshRenderer == null)
        {
            meshRenderer = GetComponent<Renderer>();
        }

        Texture mainTexture = meshRenderer.material.mainTexture;
        SaveTextureToFile(TextureToTexture2D(mainTexture), Application.persistentDataPath + AnimalsManager.FolderWithTextures, AnimalsManager.GetAnimalFileName(CurrentAnimalType));

        renderCamera.SetActive(true);
    }

    private Texture2D TextureToTexture2D(Texture texture)
    {
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);

        RenderTexture renderTexture = new RenderTexture(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);

        RenderTexture.active = renderTexture;

        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();
        return texture2D;
    }

    private void SaveTextureToFile(Texture2D texture, string path, string fileName)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        File.WriteAllBytes(path + fileName, texture.EncodeToPNG());
    }
}
