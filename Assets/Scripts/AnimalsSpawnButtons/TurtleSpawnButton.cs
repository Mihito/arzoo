﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TurtleSpawnButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject ModelPrefab;
    public GameObject CurrentModel;
    public AnimalsManager.Animal animalType;
    public Renderer CurrentModelRenderer;
  //  private readonly Vector3 turtleStartingPosition = new Vector3(46, 7.2f, 61);

    private bool followTouch;

    private void Start()
    {
        SetupTexture();
    }

    private Vector3 GetStartingPosition(AnimalsManager.Animal animal)
    {
        switch (animal)
        {
            case AnimalsManager.Animal.Turtle:
                return new Vector3(46.13f, 6.8f, 61.3f);
            case AnimalsManager.Animal.Horse:
                return new Vector3(48.67f, 6.83f, 59f);
            case AnimalsManager.Animal.Elephant:
                return new Vector3(50.9f, 6.91f, 56.2f);
            case AnimalsManager.Animal.Rhino:
                return new Vector3(44, 6.79f, 64.58f);
            case AnimalsManager.Animal.Rabbit:
                return new Vector3(41.19f, 6.9f, 67.26f);
            case AnimalsManager.Animal.Tiger:
                return new Vector3(42.95f, 9.81f, 68.55f);
            default:
                return new Vector3(46, 7.2f, 61);
        }
    }

    private void SetupTexture()
    {
        CurrentModelRenderer = CurrentModel.GetComponent<Renderer>();
   //     CurrentModelRenderer.material.mainTexture = AnimalsManager.Instance.GetTexture(animalType);
   //     if (CurrentModelRenderer.material.mainTexture == null)
   //     {
    //        print("CurrentModelRenderer.material.mainTexture is null");
            StartCoroutine(WaitForTexture());
   //     }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        followTouch = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        followTouch = false;
        Ray vRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(vRay, out hit))
        {
            var placeForAnimal = hit.transform.GetComponent<PlaceForAnAnimal>();
            if (placeForAnimal != null && placeForAnimal.GetAnimal() == null)
            {
                Debug.Log("Did Hit " + placeForAnimal);
                placeForAnimal.PutAnAnimal(CurrentModel.transform);
                CurrentModel.transform.position = placeForAnimal.transform.position;
                CurrentModel.layer = 0;
                var newModel = Instantiate(ModelPrefab, GetStartingPosition(animalType), Quaternion.identity);
                newModel.transform.localScale = Vector3.one * 2;
                CurrentModel = newModel;
                SetupTexture();
            }
            else
            {
                Debug.Log("Did not Hit placeForAnimal");
                CurrentModel.transform.position = GetStartingPosition(animalType);
                CurrentModel.transform.localScale = Vector3.one * 2;
            }
        }
        else
        {
            Debug.Log("Did not Hit anything");
            CurrentModel.transform.position = GetStartingPosition(animalType);
            CurrentModel.transform.localScale = Vector3.one * 2;
        }
    }

    private void Update()
    {
        if (followTouch)
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            var targetPosition = Input.mousePosition;
            targetPosition.z = 10;
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(targetPosition);
            CurrentModel.transform.position = new Vector3(touchPos.x, touchPos.y, touchPos.z);
            CurrentModel.transform.localScale = Vector3.one;
#else
            Vector3 targetPosition = Input.GetTouch(0).position;
            targetPosition.z = 10;
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(targetPosition);
            CurrentModel.transform.position = new Vector3(touchPos.x, touchPos.y, touchPos.z);
            CurrentModel.transform.localScale = Vector3.one;
#endif
        }
    }

    private IEnumerator WaitForTexture()
    {
        if (!CurrentModelRenderer)
        {
            CurrentModelRenderer = CurrentModel.GetComponent<Renderer>();
        }

        if (CurrentModelRenderer.material.mainTexture != null)
        {
            yield break;
        }

        while (AnimalsManager.Instance.GetTexture(animalType) == null)
        {
            yield return new WaitForSeconds(0.5f);
        }

        print("Animal Texture loaded");
        CurrentModelRenderer.material.mainTexture = AnimalsManager.Instance.GetTexture(animalType);
    }
}
