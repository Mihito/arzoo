﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AnimalsManager : MonoBehaviour
{
    public static AnimalsManager Instance;

    public Transform HungerStatusesKeeper;
    public HungerStatus HungerStatusPrefab;
    public Texture2D TurtleTexture;
    public Texture2D ElephantTexture;
    public Texture2D RhinoTexture;
    public Texture2D RabbitTexture;
    public Texture2D HorseTexture;
    public Texture2D TigerTexture;


    public enum Animal
    {
        Turtle,
        Horse, Elephant, Rhino, Rabbit, Tiger
    }

    public const string FolderWithTextures = "/SavedTextures/";

    [SerializeField] private List<Renderer> turtleRenderers;
    [SerializeField] private Button turtleSpawnButton;

    [SerializeField] private List<Renderer> horseRenderers;
    [SerializeField] private Button horseSpawnButton;

    [SerializeField] private List<Renderer> elephantRenderers;
    [SerializeField] private Button elephantSpawnButton;

    [SerializeField] private List<Renderer> rhinoRenderers;
    [SerializeField] private Button rhinoSpawnButton;

    [SerializeField] private List<Renderer> rabbitRenderers;
    [SerializeField] private Button rabbitSpawnButton;

    [SerializeField] private List<Renderer> tigerRenderers;
    [SerializeField] private Button tigerSpawnButton;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        LoadAnimals();
    }

    public Texture2D GetTexture(Animal animal)
    {
        switch (animal)
        {
            case Animal.Turtle:
                return TurtleTexture;
            case Animal.Horse:
                return HorseTexture;
            case Animal.Elephant:
                return ElephantTexture;
            case Animal.Rhino:
                return RhinoTexture;
            case Animal.Rabbit:
                return RabbitTexture;
            case Animal.Tiger:
                return TigerTexture;
            default:
                return null;
        }
    }

    public static string GetAnimalFileName(Animal animal)
    {
        switch (animal)
        {
            case Animal.Turtle:
                return "turtle.png";
            case Animal.Horse:
                return "horse.png";
            case Animal.Elephant:
                return "elephant.png";
            case Animal.Rhino:
                return "rhino.png";
            case Animal.Rabbit:
                return "rabbit.png";
            case Animal.Tiger:
                return "tiger.png";
            default:
                throw new ArgumentOutOfRangeException("animal", animal, null);
        }
    }

    private void LoadAnimals()
    {
        LoadAnimal(Animal.Turtle);
        LoadAnimal(Animal.Horse);
        LoadAnimal(Animal.Tiger);
        LoadAnimal(Animal.Rhino);
        LoadAnimal(Animal.Rabbit);
        LoadAnimal(Animal.Elephant);
    }

    private void LoadAnimal(Animal animal)
    {
        switch (animal)
        {
            case Animal.Turtle:

                TurtleTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var turtle in turtleRenderers)
                {
                    turtle.material.mainTexture = TurtleTexture;
                }
                StartCoroutine(LoadTextureDelayed());
                break;

            case Animal.Horse:

                HorseTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var horse in horseRenderers)
                {
                    horse.material.mainTexture =
                        LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                    horse.material.shader = Shader.Find("Mobile/VertexLit");
                }
                break;

            case Animal.Rabbit:

                RabbitTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var rabbit in rabbitRenderers)
                {
                    rabbit.material.mainTexture =
                        LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                    rabbit.material.shader = Shader.Find("Mobile/VertexLit");
                }
                break;

            case Animal.Tiger:

                TigerTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var tiger in tigerRenderers)
                {
                    tiger.material.mainTexture =
                        LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                    tiger.material.shader = Shader.Find("Mobile/VertexLit");
                }
                break;

            case Animal.Elephant:

                ElephantTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var elephant in elephantRenderers)
                {
                    elephant.material.mainTexture =
                        LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                    elephant.material.shader = Shader.Find("Mobile/VertexLit");
                }
                break;

            case Animal.Rhino:

                RhinoTexture =
                    LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                foreach (var rhino in rhinoRenderers)
                {
                    rhino.material.mainTexture =
                        LoadPNG(Application.persistentDataPath + FolderWithTextures + GetAnimalFileName(animal));
                    rhino.material.shader = Shader.Find("Mobile/VertexLit");
                }
                break;

            default:
                throw new ArgumentOutOfRangeException("animal", animal, null);
        }
    }

    private IEnumerator LoadTextureDelayed()
    {
        if (TurtleTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach(var turtle in turtleRenderers)
        {
            if(turtle.material.mainTexture == null)
            turtle.material.mainTexture = TurtleTexture;
        }

        if (HorseTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach (var horse in turtleRenderers)
        {
            if (horse.material.mainTexture == null)
                horse.material.mainTexture = HorseTexture;
        }

        if (RhinoTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach (var rhino in turtleRenderers)
        {
            if (rhino.material.mainTexture == null)
                rhino.material.mainTexture = RhinoTexture;
        }

        if (RabbitTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach (var rabbit in turtleRenderers)
        {
            if (rabbit.material.mainTexture == null)
                rabbit.material.mainTexture = RabbitTexture;
        }

        if (TigerTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach (var tiger in turtleRenderers)
        {
            if (tiger.material.mainTexture == null)
                tiger.material.mainTexture = TigerTexture;
        }

        if (ElephantTexture == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        foreach (var elephant in turtleRenderers)
        {
            if (elephant.material.mainTexture == null)
                elephant.material.mainTexture = ElephantTexture;
        }
    }

    private Texture2D LoadPNG(string path)
    {
        Texture2D tex = null;

        if(File.Exists(path))
        {
            var bytes = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(bytes);
        }
        else
        {
            Debug.LogError(path + " file doesn't exists");
        }

        return tex;
    }
}
