﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotateAroundButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Vector3 rotateVector;
    [SerializeField] private List<Transform> animalsModelsToRotate = new List<Transform>();

    private bool rotate = false;

    void Update()
    {
        if (rotate)
        {
            foreach (Transform model in animalsModelsToRotate)
            {
                model.Rotate(rotateVector * Time.deltaTime, Space.Self);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        rotate = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        rotate = false;
    }
}
